library(httr)
library(jsonlite)
library(googleAuthR)
library(RGA)
library(devtools)

#global authentication token
connection_token <- ""

# open new connection and authenticate user
open <- function(app_id, app_secret){
  #authenticate application using clientID and secret
  pophr <- tryCatch({
    oauth_app("google", key=app_id, secret=app_secret)
  }, warning = function(w){
    print(w)
  }, error = function(e){
    print(e)
    print("ERROR: The provided client ID and/or secret is incorrect.")
  })
  
  #create google authentication token - opens web browser for user loging and key generation
  #user must copy key into R console to continue
  
  google_token <- tryCatch({
    oauth2.0_token(oauth_endpoints("google"), pophr, scope=c("https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/webmasters", "https://www.googleapis.com/auth/analytics"))
  }, warning = function(w){
    print(w)
  }, error = function(e){
    print(e)
    print("ERROR: Connection to PopHR Server failed.")
  })
  
  #Establish authentication token as global var - To be used with other library functions
  connection_token <<- google_token
  print("Connection to PopHR Server established.")
}

close <- function(){
  #Disconnect from server by resetting global connection_token, requires user to run open() to establish new connection
  connection_token <<- ""
  print("Disconnected from PopHR Server.")
}

retrieveIndicator <-function(){
  #Get indicator data, use connection_token to authenticate access to server.
  if(typeof(connection_token) == "character"){ #Check if connection_token has been initialized to a google token
    stop("User must authenticate by connecting to the PopHR Server. Use open(clientId, clientSecret) to establish connection.", call. = FALSE)
  }
  
  #Create GET query and request with connection_token
  #(TODO: Add params for query, build url with these params)
  req <- GET("http://pophr-dev.mchi.mcgill.ca/pophr/indicator/phio:DiabetesPrevalenceIndicator?d=phio:stratification_CLSC", config(token=connection_token))
  
  #Error checks for API response
  if(http_error(req)){
    stop(
      sprintf(
        "PopHR API request failed [%s]\n<%s>",
        status_code(req),
        req$url
      ),
      call. = FALSE
    )
  }
  else if(http_type(req) != "application/json"){
    stop("API did not return json.", call. = FALSE)
  }
  
  #Format query output for user
  #(TODO: Prettify R object for more usable data)
  output <- fromJSON(content(req,"text"))
  return(output)
}


